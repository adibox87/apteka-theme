// You can extend default webpack build here. Read more on docs: https://github.com/DivanteLtd/vue-storefront/blob/master/doc/Working%20with%20webpack.md
module.exports = function (config, { isClient, isDev }) {
  config.default.module.rules.push({
    test: /\.svg$/,
    use: [
      'babel-loader',
      'vue-svg-loader',
    ]
  });
  // console.log(config.default.module.rules);

  return config;
};
