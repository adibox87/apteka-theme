/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    container: {
      padding: '15px'
    },
    extend: {
      colors: {
        primary: '#C80000'
      },
      spacing: {
        '23': '5.75rem'
      },
      borderRadius: {
        xl: '1rem',
        '2xl': '1.5rem'
      },
      boxShadow: {
        '4xl': '0 40px 120px rgba(0, 0, 0, 0.1)'
      }
    }
  },
  variants: {
    padding: ['last']
  },
  plugins: []
};
